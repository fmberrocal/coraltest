package com.portal.app.Exercise.Pages;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import com.portal.app.AbstractPage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class PageObject1 extends AbstractPage {
    //HomePage
    @FindBy(css = "#logo")
    private WebElement SITE_LOGO;
    @FindBy(css = "#top-login-form .login-button")
    private WebElement HEADER_LOGIN_BUTTON;
    @FindBy(css = "#top-login-form .join-button")
    private WebElement HEADER_JOIN_BUTTON;

    @FindBy(css = ".switchers .ng-scope .ng-isolate-scope")
    private WebElement SWITCH_BUTTON;


    private Logger log = LogManager.getLogger(com.portal.app.Exercise.Pages.PageObject.class.getName());

    private final String logedInCookieName = "pas[galabingo][real][isOnline]";

    WebDriverWait wait = new WebDriverWait(driver,30);

    private WebElement w;
    private String selectedbet;
    private String stakew;

    public PageObject1() {
        PageFactory.initElements(driver, this);
    }

    public void openPage( String url1 ) {
        super.goTo(url1);
    }

    public Boolean isCoral(){
        log.info("Checking if Login and Join Buttons are present");
        try{
            wait.until(ExpectedConditions.visibilityOf(HEADER_LOGIN_BUTTON));
            Assert.assertTrue(HEADER_JOIN_BUTTON.isDisplayed(),"Login Button is there but Join Button not");
            ATUReports.add("LOGIN AND JOIN HEADER BUTTONS ARE PRESENT", LogAs.PASSED,null);
            return true;
        }catch (Exception ex){
            log.info(ex);
            ATUReports.add("LOGIN AND JOIN HEADER BUTTONS ARE NOT PRESENT", LogAs.FAILED,new CaptureScreen(CaptureScreen.ScreenshotOf.BROWSER_PAGE));
            return false;
        }
    }

    public void splashClose(){
        log.info("Closing Splash Page");
        SITE_LOGO.click();
        ATUReports.add("SPLASH PAGE LOGO CLICKED", LogAs.PASSED,null);
    }

    public void goLeftMenu(String st1)
    {
        w=driver.findElement(By.linkText(st1));
        w.click();
    }

    public void selectBet(String bet)
    {
        w = driver.findElement(By.xpath("//*div[@class='bet-title']/span[@title='"+bet+"']/following-sibling::div[@class='home-odds']/div[@class='bet-price']"));
        String[] parts = bet.split(" v ");
        this.selectedbet = parts[0];
        w.click();
    }
    public void setStakes(String stakes)
    {
        w = driver.findElement(By.name("spl0"));
        w.sendKeys(String.valueOf(stakes));
        this.stakew = String.valueOf(stakes);

    }
    public void confirmBet()
    {
        w = driver.findElement(By.className("bs-button bs-login-and-bet"));
        w.click();
        waitForPageLoaded(driver,60);
        w = driver.findElement(By.id("bs-continue-submit"));
        w.click();
        //
    }

    public void openBetSlip()
    {
        w = driver.findElement(By.id("bs-collapsed-selections-count-inner"));
        w.click();
        w = driver.findElement(By.id("bs-view-tab-openbets"));
        w.click();

    }
    public boolean ensureBet()
    {
        String namebet;
        String stake;
        namebet = driver.findElement(By.xpath("/*div[@class='bs-item bs-bet']/div[@class='bs-bet-row']/span[@class='bs-selection-name wide-bs-selection-name']")).getText();
        stake = driver.findElement(By.xpath("/*div[@class='bs-item bs-bet']/div[@class='bs-bet-row']/div[@class='bs-stake']/span[@class='bs-stake-value']")).getText();

        if(namebet == this.selectedbet && stake == this.stakew) return true; else return false;

    }
    public void goToMenu()
    {
        w = driver.findElement(By.xpath("//*span[@title='Football']"));
        w.click();
    }

    public  boolean ensureTag()
    {

        log.info("Checking if Login and Join Buttons are present");
        try{
            wait.until(ExpectedConditions.visibilityOf(HEADER_LOGIN_BUTTON));
            Assert.assertTrue(SWITCH_BUTTON.isDisplayed(),"TAG MATCHES IS  SELECTED");
            ATUReports.add("TAG MATCHES IS  SELECTED", LogAs.PASSED,null);
            return true;
        }catch (Exception ex) {
            log.info(ex);
            ATUReports.add("TAG MATCHES IS NOT SELECTED", LogAs.FAILED, new CaptureScreen(CaptureScreen.ScreenshotOf.BROWSER_PAGE));
            return false;
        }
    }
    public void selectBetMobile(String bet)
    {
        String[] parts = bet.split(" v ");
        this.selectedbet = parts[0];
        w = driver.findElement(By.xpath("//*div[@class='odds-right']/div[@class='odds-btn-content' and contains @class='ng-scope']/div[@class='odds-btn-content' and contains @class='ng-scope']/button[@class='btn-bet']"));
        w.click();
    }
    public void setStakesMobile(String stakes)
    {
        w = driver.findElement(By.xpath("//*button[@text='£"+String.valueOf(stakes)+"']"));
        w.click();

    }
    public void confirmBetMobile()
    {
        w = driver.findElement(By.className("betnow-btn"));
        w.click();
        waitForPageLoaded(driver,60);
        w = driver.findElement(By.className("done-btn"));
        w.click();
    }
    public void openBetSlipMobile()
    {
        w = driver.findElement(By.xpath("//*a[@data-crlat='myBetsBtn']"));
        w.click();

    }

    public void loginMobile()
    {
        w = driver.findElement(By.xpath("//*a[@data-i18n='bma.signIn']"));
        w.click();
        w = driver.findElement(By.xpath("//*input[@name='username']"));
        w.sendKeys("uatbettingauto1");
        w = driver.findElement(By.xpath("//*input[@name='password']"));
        w.sendKeys("example");
        w = driver.findElement(By.xpath("//*button[@data-crlat='loginButton']"));
        w.click();
    }

    public boolean is_login()
    {
        log.info("Checking if Login and Join Buttons are present");
        try{
            w = driver.findElement(By.xpath("//*a[@data-i18n='bma.signIn']"));
            wait.until(ExpectedConditions.visibilityOf(w));
            Assert.assertTrue(!(w.isDisplayed()),"Login Button has dissapear");
            ATUReports.add("LOGIN  BUTTON ARE NOT PRESENT", LogAs.PASSED,null);
            return true;
        }catch (Exception ex){
            log.info(ex);
            ATUReports.add("LOGIN  ARE  PRESENT", LogAs.FAILED,new CaptureScreen(CaptureScreen.ScreenshotOf.BROWSER_PAGE));
            return false;
        }

    }

    public void logoutMobile()
    {
        w = driver.findElement(By.xpath("//*a[@data-uat='secondBtn']"));
        w.click();
        w = driver.findElement(By.xpath("//*a[@title='Logout']"));
        w.click();
    }

}

