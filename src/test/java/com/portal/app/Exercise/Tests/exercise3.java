package com.portal.app.Exercise.Tests;

import com.portal.app.AbstractTest;
import com.portal.app.Exercise.Pages.PageObject1;
import com.portal.app.RetryAnalyzerImpl;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;
import spring.User;

import com.portal.app.AbstractTest;
import com.portal.app.Exercise.Pages.PageObject1;
import com.portal.app.RetryAnalyzerImpl;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;
import spring.User;

import com.portal.app.AbstractTest;
import com.portal.app.Exercise.Pages.PageObject1;
import com.portal.app.RetryAnalyzerImpl;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;
import spring.User;
public class exercise3 extends AbstractTest {

    private Logger log = LogManager.getLogger(com.portal.app.Exercise.Tests.exercise3.class.getName());

    @Autowired
    @Qualifier("testBean")

    @org.testng.annotations.Test(retryAnalyzer=RetryAnalyzerImpl.class)
    public void exer3()  {
        PageObject1 po = new PageObject1();
        po.openPage("https://bet.coral.co.uk/#/"); //open the page
        Assert.assertTrue(po.isCoral(), "No user is logged in");//assert with user is not logged
        po.loginMobile();
        Assert.assertTrue( po.is_login(), "The user is login");
        po.logoutMobile();
        Assert.assertTrue( !(po.is_login()), "The user is logout");
    }
}
