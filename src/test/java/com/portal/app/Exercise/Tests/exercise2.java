package com.portal.app.Exercise.Tests;

import com.portal.app.AbstractTest;
import com.portal.app.Exercise.Pages.PageObject1;
import com.portal.app.RetryAnalyzerImpl;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;
import spring.User;

import com.portal.app.AbstractTest;
import com.portal.app.Exercise.Pages.PageObject1;
import com.portal.app.RetryAnalyzerImpl;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;
import spring.User;
public class exercise2 extends AbstractTest {

    private Logger log = LogManager.getLogger(com.portal.app.Exercise.Tests.exercise1.class.getName());

    @Autowired
    @Qualifier("testBean")
    private User user;

    @org.testng.annotations.Test(retryAnalyzer=RetryAnalyzerImpl.class)
    public void exer2()  {
        PageObject1 po = new PageObject1();
        po.openPage("https://bet.coral.co.uk/#/"); //open the page
        Assert.assertTrue(po.isCoral(), "No user is logged in");//assert with user is not logged
        po.ensureTag();
        po.selectBetMobile("Bristol City");
        po.setStakesMobile("10");
        user.setUsername("uatbettingauto1");
        user.setPassword("example");
        po.confirmBetMobile();
        po.openBetSlipMobile();
        Assert.assertTrue(po.ensureBet(), " The selected bet is correct");
    }
}
